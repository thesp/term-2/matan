if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
inoremap <D-BS> 
inoremap <M-BS> 
inoremap <M-Down> }
inoremap <D-Down> <C-End>
inoremap <M-Up> {
inoremap <D-Up> <C-Home>
noremap! <M-Right> <C-Right>
noremap! <D-Right> <End>
noremap! <M-Left> <C-Left>
noremap! <D-Left> <Home>
inoremap <silent> <C-Tab> =UltiSnips#ListSnippets()
inoremap <F10>  :w  :!clang++ -fsanitize=address -std=c++17 -Wall -Wextra -Wshadow -DONPC -O0 -o "%<" "%" && "./%<" < inp
inoremap <F9>  :w  :!clang++ -fsanitize=address -std=c++17 -Wall -Wextra -Wshadow -DONPC -O0 -o %< % && ./%< 
inoremap <F8>  :w  :!clang++ -fsanitize=address -std=c++17 -DONPC -O0 -o "%<" "%" && "./%<" < inp
inoremap <F5>  :w  :!pytest %<.py 
inoremap <F4>  :w  :!pycodestyle %<.py 
inoremap <F3>  :w  :make 
inoremap <F1> 
snoremap <silent>  c
xnoremap <silent> 	 :call UltiSnips#SaveLastVisualSelection()gvs
snoremap <silent> 	 :call UltiSnips#ExpandSnippetOrJump()
nnoremap 	 %
onoremap 	 %
snoremap  "_c
noremap  s :source ~/.vimrc 
noremap  / 0i//
noremap  a ggVG
noremap  i 	
noremap  o 
noremap  q :q
noremap  n :vs 
noremap  u i_r
noremap  j <NL>
noremap  r 
noremap  e :e .
noremap  b ^
xmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
noremap <M-Down> }
noremap <D-Down> <C-End>
noremap <M-Up> {
noremap <D-Up> <C-Home>
noremap <M-Right> <C-Right>
noremap <D-Right> <End>
noremap <M-Left> <C-Left>
noremap <D-Left> <Home>
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(expand((exists("g:netrw_gx")? g:netrw_gx : '<cfile>')),netrw#CheckIfRemote())
snoremap <silent> <Del> c
snoremap <silent> <BS> c
snoremap <silent> <C-Tab> :call UltiSnips#ListSnippets()
noremap <F10>  :w  :!clang++ -fsanitize=address -std=c++17 -Wall -Wextra -Wshadow -DONPC -O0 -o %< % && ./%< < inp
noremap <F9>  :w  :!clang++ -fsanitize=address -std=c++17 -Wall -Wextra -Wshadow -DONPC -O0 -o %< % && ./%< 
noremap <F8>  :w  :!clang++ -fsanitize=address -std=c++17 -DONPC -O0 -o %< % && ./%< < inp
noremap <F7>  :w !python3 
noremap <F5>  :w  :!pytest %<.py 
noremap <F4>  :w  :!pycodestyle %<.py 
noremap <F3>  :w  :make 
vnoremap <F1> :tabprev 
nnoremap <F1> :tabprev 
onoremap <F1> :tabprev 
xmap <BS> "-d
inoremap <silent> 	 =UltiSnips#ExpandSnippetOrJump()
inoremap { {}k$A
let &cpo=s:cpo_save
unlet s:cpo_save
set autoindent
set autoread
set background=dark
set backspace=indent,eol,start
set cindent
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set guifont=Menlo\ Regular:h16
set guitablabel=%M%t
set helplang=ru
set history=1000
set hlsearch
set ignorecase
set incsearch
set iskeyword=@,48-57,_,192-255,:
set langmenu=none
set mouse=a
set printexpr=system('open\ -a\ Preview\ '.v:fname_in)\ +\ v:shell_error
set pyxversion=2
set runtimepath=~/.vim,~/.vim/plugged/vimtex/,~/.vim/plugged/ultisnips/,~/.vim/plugged/vim-snippets/,~/.vim/plugged/vim-lldb/,/Applications/MacVim.app/Contents/Resources/vim/vimfiles,/Applications/MacVim.app/Contents/Resources/vim/runtime,/Applications/MacVim.app/Contents/Resources/vim/vimfiles/after,~/.vim/plugged/vimtex/after,~/.vim/plugged/ultisnips/after,~/.vim/after
set shiftwidth=4
set smarttab
set suffixes=.bak,~,.o,.h,.info,.swp,.obj,.sty,.cls,.log,.aux,.bbl,.out,.blg,.brf,.cb,.dvi,.fdb_latexmk,.fls,.idx,.ilg,.ind,.inx,.pdf,.synctex.gz,.toc
set noswapfile
set tabstop=4
set termencoding=utf-8
set visualbell
set window=45
" vim: set ft=vim :
